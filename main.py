# required libraries
import pandas as pd
import matplotlib.pyplot as plt
from tabulate import tabulate
from math import sqrt
import datetime
import numpy as np
from scipy.stats import gaussian_kde


def load_data():
    """
    loading data from csv file

    :return: data
    """
    # Read data from file 'covid.csv'
    data = pd.read_csv("covid.csv")
    print("Data loaded from file")

    return data


def show_data(data):
    """
    Show data as a table

    :param data: data that loaded
    :return: nothing
    """
    print(tabulate(data, headers='keys', tablefmt='psql', showindex=False))
    print()


def show_birth_year_feature(data):
    """
    Find and show "min", "max" and "std" of birth year of data
    :param data: data that loaded
    :return: nothing
    """

    # Initialize
    max_birth_year = 0
    sum_birth_year = 0
    number_birth_year = 0
    birth_year_values = data.loc[:, 'birth_year'].values
    for birth_year_value in birth_year_values:
        try:
            birth_year = int(birth_year_value)
            if birth_year > max_birth_year:
                max_birth_year = birth_year

            sum_birth_year += birth_year
            number_birth_year += 1
        except ValueError:
            # value is null. Ignore It!
            pass
    mean_birth_year = sum_birth_year / number_birth_year

    sum = 0
    for birth_year_value in birth_year_values:
        try:
            birth_year = int(birth_year_value)
            sum += pow(birth_year - mean_birth_year, 2)
        except ValueError:
            # value is null. Ignore It!
            pass

    std_birth_year = sqrt(sum / (number_birth_year - 1))

    # Show values
    print(f"max birth year: {max_birth_year}")
    print(f"mean birth year: {int(mean_birth_year)}")
    print(f"std birth year: {std_birth_year}\n")


def resolve_null_values(data):
    """
    Resolve null values in data
    :param data: data that loaded
    :return: processed data
    """
    # Remove data that birth_year or region equals to null
    processed_data = data.dropna(subset=['birth_year', 'region'])

    # Remove columns infection_reason, infected_by
    del processed_data['infection_reason']
    del processed_data['infected_by']

    return processed_data


def visualization(data):
    """
    Visualize data with some plots

    :param data: data that processed
    :return: nothing
    """
    visualization_histogram_plot(data)
    visualization_scatter_plot(data)
    visualization_matrix_plot(data)


def visualization_histogram_plot(data):
    """
    Visualization data with histogram plot

    :param data: data that processed
    :return: nothing
    """

    # Confirmed date plot
    confirmed_date_string_list = data['confirmed_date'].values.tolist()
    confirmed_date_list = list()

    for confirmed_date in confirmed_date_string_list:
        # Convert date to day of year
        date = datetime.datetime.strptime(confirmed_date, "%m/%d/%Y")
        day_of_year = date.timetuple().tm_yday
        confirmed_date_list.append(day_of_year)

    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.title.set_text('Confirmed date')
    ax1.hist(confirmed_date_list, color='red')

    # Birth year plot
    birth_year_list = data['birth_year'].values.tolist()

    ax2 = fig.add_subplot(212)
    ax2.title.set_text('Birth year')
    ax2.hist(birth_year_list, color='green')

    plt.show()


def visualization_scatter_plot(data):
    """
    Visualization data with scatter plot

    :param data: data that processed
    :return: nothing
    """
    confirmed_date_string_list = data['confirmed_date'].values.tolist()
    confirmed_date_list = list()
    for confirmed_date in confirmed_date_string_list:
        # Convert date to day of year
        date = datetime.datetime.strptime(confirmed_date, "%m/%d/%Y")
        day_of_year = date.timetuple().tm_yday
        confirmed_date_list.append(day_of_year)

    region_data_list = data['region'].values.tolist()

    plt.scatter(confirmed_date_list, region_data_list)
    plt.show()


def visualization_matrix_plot(data):
    """
    Visualization data with matrix plot

    :param data: data that processed
    :return: nothing
    """

    # find birth decay
    data['birth_decade'] = data['birth_year'] / 10
    data['birth_decade'] = data['birth_decade'].astype(int)
    data['birth_decade'] = data['birth_decade'] * 10

    data_list = data[['birth_decade', 'state']].values.tolist()

    aggregation_data = data.groupby(['state', 'birth_decade'], as_index=False).count()
    birth_decade_list = aggregation_data['birth_decade'].tolist()
    state_list = aggregation_data['state'].tolist()
    counter_list = list()

    color_list = list()
    for i in range(len(state_list)):
        state = state_list[i]
        birth_decade = birth_decade_list[i]
        counter = 0
        for item in data_list:
            if item[0] == birth_decade and item[1] == state:
                counter += 1
        counter_list.append(counter)
        if 1 <= counter <= 5:
            color = "blue"
        elif counter <= 10:
            color = "green"
        elif counter <= 15:
            color = "yellow"
        elif counter <= 20:
            color = "orange"
        else:
            color = "red"

        color_list.append(color)

    plt.scatter(birth_decade_list, state_list, color=color_list, s=800)
    plt.show()


if __name__ == '__main__':
    data = load_data()
    show_data(data)
    show_birth_year_feature(data)
    data = resolve_null_values(data)
    show_data(data)
    visualization(data)
